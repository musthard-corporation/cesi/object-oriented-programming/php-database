<html>
    <head>
        <title>Product - List bags</title>
    </head>
    <body>
        <a href="index.php">Accueil</a>
        <table>
            <thead>
                <tr>
                    <td>id</td>
                    <td>price</td>
                    <td>model</td>
                    <td>color</td>
                    <td>with_handle</td>
                    <td>shoulder_strap</td>
                    <td></td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                <?php
                include("models/init_db.php");
                $pdo = init_db();

                $unbufferedResult = $pdo->query("SELECT * FROM bags");
                foreach ($unbufferedResult as $row) {
                    ?>
                        <tr>
                            <td><?php echo $row['id'] ?></td>
                            <td><?php echo $row['price'] ?></td>
                            <td><?php echo $row['model'] ?></td>
                            <td><?php echo $row['color'] ?></td>
                            <td><?php echo $row['with_handle'] ? 'Avec' : 'Sans' ?></td>
                            <td><?php echo $row['shoulder_strap'] ? 'Avec' : 'Sans' ?></td>
                            <td><a href="/update_bag.php?id=<?php echo $row['id'] ?>">Modifier</a></td>
                            <td><a href="/delete_bag.php?id=<?php echo $row['id'] ?>">Supprimer</a></td>
                        </tr>
                        <?php
                }
                ?>
            </tbody>
        </table>
    </body>
</hmtl>