
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    createBag();
}

function createBag() {
    $price = $_POST['price'];
    $model = $_POST['model'];
    $color = $_POST['color'];
    $with_handle = empty($_POST['with_handle']) ? false : true;
    $shoulder_strap = empty($_POST['shoulder_strap']) ? false : true;

    include("models/init_db.php");
    $pdo = init_db();

    $stmt = $pdo->prepare("INSERT INTO bags(price, model, color, with_handle, shoulder_strap) VALUES (?, ?, ?, ?, ?)");
    $stmt->bindParam(1, $price);
    $stmt->bindParam(2, $model);
    $stmt->bindParam(3, $color);
    $stmt->bindParam(4, $with_handle, PDO::PARAM_BOOL);
    $stmt->bindParam(5, $shoulder_strap, PDO::PARAM_BOOL);
    $stmt->execute();

    header("Location: list_bags.php");
}
?>
<html>
    <head>
        <title>Product - Create Bag</title>
    </head>
    <body>
        <a href="index.php">Accueil</a>
        <form method="POST">
            <div class="form-field">
                <label for="price">Prix</label>
                <input type="number" name="price" required>
            </div>
            <div class="form-field">
                <label for="model">Modèle</label>
                <input type="text" name="model" required>
            </div>
            <div class="form-field">
                <label for="color">Couleur</label>
                <input type="text" name="color" required>
            </div>
            <div class="form-field">
                <label for="with_handle">Avec des anses ?</label>
                <input type="checkbox" name="with_handle">
            </div>
            <div class="form-field">
                <label for="shoulder_strap">En bandoulière ?</label>
                <input type="checkbox" name="shoulder_strap">
            </div>
            <input type="submit" value="Enregistrer">
        </form>
    </body>
</hmtl>