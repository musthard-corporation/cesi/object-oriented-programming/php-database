<?php
include("models/bag.php");
include("models/init_db.php");
$pdo = init_db();

$id;
$bag;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $id = $_PUT['id'];
    updateBag($pdo, $id);
} else {
    $id = $_GET['id'];
    $bag = getBag($pdo, $id);
}

if (!isset($id)) {
    header("Location: list_bags.php");
}

function getBag($pdo, $id) {
    $stmt = $pdo->prepare("SELECT * FROM bags WHERE id = ?");
    $stmt->execute(array($id));
    $row = $stmt->fetch();
    return new Bag(
        $row['id'],
        $row['price'],
        $row['model'],
        $row['color'],
        $row['with_handle'],
        $row['shoulder_strap']
    );
}

function updateBag($pdo, $id) {
    $price = $_PUT['price'];
    $model = $_PUT['model'];
    $color = $_PUT['color'];
    $with_handle = empty($_PUT['with_handle']) ? false : true;
    $shoulder_strap = empty($_PUT['shoulder_strap']) ? false : true;

    $stmt = $pdo->prepare("UPDATE bags SET
        price = ?, model = ?, color = ?, with_handle = ?, shoulder_strap = ?
        WHERE id = ?");
    $stmt->bindParam(1, $price);
    $stmt->bindParam(2, $model);
    $stmt->bindParam(3, $color);
    $stmt->bindParam(4, $with_handle, PDO::PARAM_BOOL);
    $stmt->bindParam(5, $shoulder_strap, PDO::PARAM_BOOL);
    $stmt->bindParam(6, $id);
    $stmt->execute();

    header("Location: update_bag.php?id=" . $id);
}
?>
<html>
    <head>
        <title>Product - Create Bag</title>
    </head>
    <body>
        <a href="index.php">Accueil</a>
        <form method="POST">
            <input type="hidden" name="id" value=<?php echo $id ?>>
            <div class="form-field">
                <label for="price">Prix</label>
                <input type="number" name="price" value="<?php echo $bag->price ?>" required>
            </div>
            <div class="form-field">
                <label for="model">Modèle</label>
                <input type="text" name="model" value="<?php echo $bag->model ?>" required>
            </div>
            <div class="form-field">
                <label for="color">Couleur</label>
                <input type="text" name="color" value="<?php echo $bag->color ?>" required>
            </div>
            <div class="form-field">
                <label for="with_handle">Avec des anses ?</label>
                <input type="checkbox" name="with_handle" checked="<?php echo $bag->with_handle ?>">
            </div>
            <div class="form-field">
                <label for="shoulder_strap">En bandoulière ?</label>
                <input type="checkbox" name="shoulder_strap" checked="<?php echo $bag->shoulder_strap ?>">
            </div>
            <input type="submit" value="Enregistrer">
        </form>
    </body>
</hmtl>