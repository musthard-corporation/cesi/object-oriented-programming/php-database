
<?php

$id = $_GET['id'];
if (isset($id)) {
    include("models/init_db.php");
    $pdo = init_db();

    $pdo->prepare("DELETE FROM bags WHERE id = ?");
    $pdo->execute(array($id));
}

header("Location: list_bags.php");
?>