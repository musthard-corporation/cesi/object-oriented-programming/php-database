<?php
class Bag {
    public int $id;
    public float $price;
    public string $model;
    public string $color;
    public bool $with_handle;
    public bool $shoulder_strap;

    public function __construct($id, $price, $model, $color, $with_handle, $shoulder_strap) {
        $this->id = $id;
        $this->price = $price;
        $this->model = $model;
        $this->color = $color;
        $this->with_handle = $with_handle;
        $this->shoulder_strap = $shoulder_strap;
    }
}
?>